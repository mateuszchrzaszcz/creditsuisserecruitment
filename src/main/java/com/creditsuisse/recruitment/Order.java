package com.creditsuisse.recruitment;

import java.util.Objects;

public class Order {
	private static final double ZERO = 0.0;

	private final String userId;
	private final double quantity;
	private final double pricePerKg;
	private final OrderType orderType;

	public Order(final String userId, final double quantity, final double pricePerKg, final OrderType orderType) {

		if(quantity <= ZERO) {
			throw new InvalidOrderQuantityException("Order quantity cannot be less or equal to 0");
		}
		if(pricePerKg <= ZERO) {
			throw new InvalidOrderPriceException("Order pricePerKg cannot be less or equal to 0");
		}

		this.userId = userId;
		this.quantity = quantity;
		this.pricePerKg = pricePerKg;
		this.orderType = orderType;
	}

	String getUserId() {
		return userId;
	}

	double getQuantity() {
		return quantity;
	}

	double getPricePerKg() {
		return pricePerKg;
	}

	OrderType getOrderType() {
		return orderType;
	}

	@Override
	public boolean equals(final Object o) {
		if (this == o) {
			return true;
		}
		if (!(o instanceof Order)) {
			return false;
		}
		final Order order = (Order) o;
		return Objects.equals(order.quantity, quantity) &&
					   Objects.equals(order.pricePerKg, pricePerKg) &&
					   Objects.equals(userId, order.userId) &&
					   Objects.equals(orderType, order.orderType);
	}

	@Override
	public int hashCode() {
		return Objects.hash(userId, quantity, pricePerKg, orderType);
	}
}
