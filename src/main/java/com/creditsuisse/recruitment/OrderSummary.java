package com.creditsuisse.recruitment;

import java.util.Objects;

class OrderSummary implements Comparable<OrderSummary> {
	private final double quantity;
	private final double pricePerKg;
	private final OrderType orderType;

	OrderSummary(final double quantity, final double pricePerKg, final OrderType orderType) {
		this.quantity = quantity;
		this.pricePerKg = pricePerKg;
		this.orderType = orderType;
	}

	double getQuantity() {
		return quantity;
	}

	double getPricePerKg() {
		return pricePerKg;
	}

	@Override
	public boolean equals(final Object o) {
		if (this == o) {
			return true;
		}
		if (!(o instanceof OrderSummary)) {
			return false;
		}
		final OrderSummary that = (OrderSummary) o;
		return Objects.equals(that.quantity, quantity) &&
				Objects.equals(that.pricePerKg, pricePerKg);
	}

	@Override
	public int hashCode() {
		return Objects.hash(quantity, pricePerKg);
	}

	@Override
	public int compareTo(final OrderSummary otherOrderSummary) {
		if (this.orderType == OrderType.SELL) {
			return orderWithLowerPricesFirst(otherOrderSummary);
		} else {
			return orderWithHigherPricesFirst(otherOrderSummary);
		}
	}

	private int orderWithLowerPricesFirst(final OrderSummary otherOrderSummary) {
		return (int) (this.pricePerKg - otherOrderSummary.pricePerKg);
	}

	private int orderWithHigherPricesFirst(final OrderSummary otherOrderSummary) {
		return (int) (otherOrderSummary.pricePerKg - this.pricePerKg);
	}
}
