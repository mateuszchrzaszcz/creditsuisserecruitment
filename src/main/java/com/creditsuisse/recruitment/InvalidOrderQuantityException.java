package com.creditsuisse.recruitment;

public class InvalidOrderQuantityException extends RuntimeException {
	public InvalidOrderQuantityException(final String message) {
		super(message);
	}
}
