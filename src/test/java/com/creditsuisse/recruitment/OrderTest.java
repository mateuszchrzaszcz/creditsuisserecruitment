package com.creditsuisse.recruitment;

import org.junit.Test;

public class OrderTest {

	private static final String VALID_ID = "someValidId";
	private static final double VALID_QUANTITY = 1.5;
	private static final int VALID_PRICE = 20;
	private static final double NEGATIVE_QUANTITY = - 2.2;
	private static final double NEGATIVE_PRICE = - 9.9;
	private static final double EQUAL_TO_ZERO_PRICE = 0.0;
	private static final double EQUAL_TO_ZERO_QUANTITY = 0.0;

	@Test
	public void shouldAllowToCreateNewValidOrder() {
		new Order(VALID_ID, VALID_QUANTITY, VALID_PRICE, OrderType.BUY);
	}

	@Test(expected = InvalidOrderQuantityException.class)
	public void shouldNotAllowToCreateOrderWithNegativeQuantity() {
		new Order(VALID_ID, NEGATIVE_QUANTITY, VALID_PRICE, OrderType.BUY);
	}

	@Test(expected = InvalidOrderQuantityException.class)
	public void shouldNotAllowToCreateOrderWithQuantityEqualToZero() {
		new Order(VALID_ID, EQUAL_TO_ZERO_QUANTITY, VALID_PRICE, OrderType.BUY);
	}

	@Test(expected = InvalidOrderPriceException.class)
	public void shouldNotAllowToCreateOrderWithNegativePrice() {
		new Order(VALID_ID, VALID_QUANTITY, NEGATIVE_PRICE, OrderType.BUY);
	}

	@Test(expected = InvalidOrderPriceException.class)
	public void shouldNotAllowToCreateOrderWithPriceEqualToZero() {
		new Order(VALID_ID, VALID_QUANTITY, EQUAL_TO_ZERO_PRICE, OrderType.SELL);
	}
}
