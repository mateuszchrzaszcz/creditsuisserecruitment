Exercise for Credit Suisse recruitment. 

Hi,

Thank you for giving me an opportunity to do this task. It was fun!

I have written it with Java8 using JUnit and AssertJ.
Upon request I can provide implementation of tests in Spock, Specs2 or ScalaTest.
Everything written in inside out TDD manner.

There were some outstanding questions regarding requirements that I could not ask you, so I went with following assumptions for the sake of this task:

1) While calling method to show summary we choose if we want summary for BUY or SELL, since there was no information 
about showing them together or whether SELL should be before BUY in summary or the other way around.

2) Connected with the first point, I do not merge orders with the same price if these orders are of different types.

3) Not sure how do you feel about defensive programming, so  I decided to go with sensible reasoning - I validate the app only for external data I have no control over (in this 
example Order)
 
4) I have written it heavily using functional side of Java8. If you would like me to use imperative approach only, I am happy to rewrite it.



Regarding what I would do differently in design, according to specification to Order class I had to add field OrderType (BUY or SELL).
If it was up to me, I would have two classes: BuyOrder and SellOrder that would be extending Order.
This way implementation of compareTo and summary could be way simpler, OOP like.

Thanks and I hope to hear from you soon! Any feedback will be useful :)

Kind Regards,
Mateusz Chrzaszcz

