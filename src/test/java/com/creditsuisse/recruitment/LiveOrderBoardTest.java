package com.creditsuisse.recruitment;

import org.junit.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class LiveOrderBoardTest {

	private static final String SOME_USER_ID = "someUserId";
	private static final String ANOTHER_USER_ID = "anotherUserId";
	private static final double SOME_QUANTITY = 3.5;
	private static final double SOME_OTHER_QUANTITY = 5.4;
	private static final int SOME_OTHER_PRICE = 305;
	private static final int SOME_PRICE = 303;
	private static final int LOWER_PRICE = 100;
	private static final int HIGHER_PRICE = 200;
	private static final OrderType SOME_VALID_TYPE = OrderType.BUY;

	@Test
	public void shouldAllowToRegisterNewOrder() {
		Order order = new Order(SOME_USER_ID, SOME_QUANTITY, SOME_PRICE, SOME_VALID_TYPE);
		OrderSummary expectedSummary = new OrderSummary(SOME_QUANTITY, SOME_PRICE, SOME_VALID_TYPE);

		LiveOrderBoard liveOrderBoard = new LiveOrderBoard();
		liveOrderBoard.register(order);

		assertThat(liveOrderBoard.summaryFor(SOME_VALID_TYPE))
				.hasSize(1)
				.contains(expectedSummary);
	}

	@Test
	public void shouldAllowToCancelParticularOrder() {
		Order order = new Order(SOME_USER_ID, SOME_QUANTITY, SOME_PRICE, SOME_VALID_TYPE);

		LiveOrderBoard liveOrderBoard = new LiveOrderBoard();
		liveOrderBoard.register(order);
		liveOrderBoard.cancelOrder(order);

		assertThat(liveOrderBoard.summaryFor(SOME_VALID_TYPE))
				.hasSize(0);
	}

	@Test
	public void shouldAllowToCancelOrderForGivenUser() {
		Order order = new Order(SOME_USER_ID, SOME_QUANTITY, SOME_PRICE, SOME_VALID_TYPE);

		LiveOrderBoard liveOrderBoard = new LiveOrderBoard();
		liveOrderBoard.register(order);
		liveOrderBoard.cancelOrder(SOME_USER_ID);

		assertThat(liveOrderBoard.summaryFor(SOME_VALID_TYPE))
				.hasSize(0);
	}

	@Test
	public void shouldAllowToRegisterMultipleOrders() {
		Order firstOrder = new Order(SOME_USER_ID, SOME_QUANTITY, SOME_PRICE, SOME_VALID_TYPE);
		Order secondOrder = new Order(ANOTHER_USER_ID, SOME_OTHER_QUANTITY, SOME_OTHER_PRICE, SOME_VALID_TYPE);
		OrderSummary expectedFirstOrderSummary = new OrderSummary(SOME_QUANTITY, SOME_PRICE, SOME_VALID_TYPE);
		OrderSummary expectedSecondOrderSummary = new OrderSummary(SOME_OTHER_QUANTITY, SOME_OTHER_PRICE, SOME_VALID_TYPE);

		LiveOrderBoard liveOrderBoard = new LiveOrderBoard();
		liveOrderBoard.register(firstOrder);
		liveOrderBoard.register(secondOrder);

		assertThat(liveOrderBoard.summaryFor(SOME_VALID_TYPE))
				.hasSize(2)
				.contains(expectedFirstOrderSummary, expectedSecondOrderSummary);
	}

	@Test
	public void shouldShowSummaryForSellOrdersWithLowestPricesDisplayedFirst() {
		Order lowerPriceOrder = new Order(SOME_USER_ID, SOME_QUANTITY, LOWER_PRICE, OrderType.SELL);
		Order higherPriceOrder = new Order(ANOTHER_USER_ID, SOME_OTHER_QUANTITY, HIGHER_PRICE, OrderType.SELL);
		OrderSummary lowerPriceOrderSummary = new OrderSummary(SOME_QUANTITY, LOWER_PRICE, OrderType.SELL);
		OrderSummary higherPriceOrderSummary = new OrderSummary(SOME_OTHER_QUANTITY, HIGHER_PRICE, OrderType.SELL);

		LiveOrderBoard liveOrderBoard = new LiveOrderBoard();
		liveOrderBoard.register(higherPriceOrder);
		liveOrderBoard.register(lowerPriceOrder);

		assertThat(liveOrderBoard.summaryFor(OrderType.SELL))
				.hasSize(2)
				.containsSequence(lowerPriceOrderSummary, higherPriceOrderSummary);
	}

	@Test
	public void shouldShowSummaryForBuyOrdersWithHighestPricesDisplayedFirst() {
		Order lowerPriceOrder = new Order(SOME_USER_ID, SOME_QUANTITY, LOWER_PRICE, OrderType.BUY);
		Order higherPriceOrder = new Order(ANOTHER_USER_ID, SOME_OTHER_QUANTITY, HIGHER_PRICE, OrderType.BUY);
		OrderSummary lowerPriceOrderSummary = new OrderSummary(SOME_QUANTITY, LOWER_PRICE, OrderType.BUY);
		OrderSummary higherPriceOrderSummary = new OrderSummary(SOME_OTHER_QUANTITY, HIGHER_PRICE, OrderType.BUY);

		LiveOrderBoard liveOrderBoard = new LiveOrderBoard();
		liveOrderBoard.register(lowerPriceOrder);
		liveOrderBoard.register(higherPriceOrder);

		assertThat(liveOrderBoard.summaryFor(OrderType.BUY))
				.hasSize(2)
				.containsSequence(higherPriceOrderSummary, lowerPriceOrderSummary);
	}

	@Test
	public void shouldShowSummaryWithOrdersHavingSamePriceBeingMergedTogether() {
		Order firstSamePriceOrder = new Order(SOME_USER_ID, SOME_QUANTITY, SOME_PRICE, SOME_VALID_TYPE);
		Order secondSamePriceOrder = new Order(ANOTHER_USER_ID, SOME_OTHER_QUANTITY, SOME_PRICE, SOME_VALID_TYPE);
		Order otherPriceOrder = new Order(SOME_USER_ID, SOME_QUANTITY, HIGHER_PRICE, SOME_VALID_TYPE);
		OrderSummary expectedMergedOrderSummary = new OrderSummary(SOME_QUANTITY + SOME_OTHER_QUANTITY, SOME_PRICE, SOME_VALID_TYPE);
		OrderSummary expectedSecondOrderSummary = new OrderSummary(SOME_QUANTITY, HIGHER_PRICE, SOME_VALID_TYPE);

		LiveOrderBoard liveOrderBoard = new LiveOrderBoard();
		liveOrderBoard.register(firstSamePriceOrder);
		liveOrderBoard.register(secondSamePriceOrder);
		liveOrderBoard.register(otherPriceOrder);

		assertThat(liveOrderBoard.summaryFor(SOME_VALID_TYPE))
				.hasSize(2)
				.containsSequence(expectedMergedOrderSummary, expectedSecondOrderSummary);
	}
}
