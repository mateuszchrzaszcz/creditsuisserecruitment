package com.creditsuisse.recruitment;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.stream.Collector;
import java.util.stream.Collectors;

public class LiveOrderBoard {

	private final List<Order> orders = new ArrayList<>();

	public void register(final Order order) {
		orders.add(order);
	}

	public void cancelOrder(final Order order) {
		orders.remove(order);
	}

	public void cancelOrder(final String userId) {
		orders.removeIf(order -> order.getUserId().equals(userId));
	}

	public List<OrderSummary> summaryFor(final OrderType orderType) {
		List<Order> filteredOrders = filterOrders(orderType);
		List<OrderSummary> orderSummaries = createOrderSummaries(filteredOrders, orderType);
		final List<OrderSummary> mergedOrderSummaries = mergeGroupedOrdersWithSamePrice(orderSummaries, orderType);
		return sortOrderSummaries(mergedOrderSummaries);
	}

	private List<Order> filterOrders(final OrderType orderType) {
		return orders.stream()
						.filter(ordersByType(orderType))
						.collect(Collectors.toList());
	}

	private Predicate<Order> ordersByType(final OrderType orderType) {
		return order -> order.getOrderType() == orderType;
	}

	private List<OrderSummary> createOrderSummaries(final List<Order> sortedOrders, final OrderType orderType) {
		return sortedOrders.stream()
						.map(transformOrderToOrderSummary(orderType))
						.collect(Collectors.toList());
	}

	private Function<Order, OrderSummary> transformOrderToOrderSummary(final OrderType orderType) {
		return order -> new OrderSummary(order.getQuantity(), order.getPricePerKg(), orderType);
	}

	private List<OrderSummary> mergeGroupedOrdersWithSamePrice(final List<OrderSummary> orderSummaries, OrderType orderType) {
		return orderSummaries.stream()
						.collect(groupByOrderPrice())
						.entrySet()
						.stream()
						.map(mergeOrdersWithSamePrice(orderType))
						.collect(Collectors.toList());
	}

	private Collector<OrderSummary, ?, Map<Double, List<OrderSummary>>> groupByOrderPrice() {
		return Collectors.groupingBy(OrderSummary::getPricePerKg);
	}

	private Function<Map.Entry<Double, List<OrderSummary>>, OrderSummary> mergeOrdersWithSamePrice(OrderType orderType) {
		return ordersGroupedByPrice -> new OrderSummary(collectTotalQuantity(ordersGroupedByPrice.getValue()), ordersGroupedByPrice.getKey(), orderType);
	}

	private double collectTotalQuantity(final List<OrderSummary> orderSummaries) {
		return orderSummaries
						.stream()
						.mapToDouble(OrderSummary::getQuantity)
						.sum();
	}

	private List<OrderSummary> sortOrderSummaries(final List<OrderSummary> mergedOrderSummaries) {
		return mergedOrderSummaries.stream()
						.sorted()
						.collect(Collectors.toList());
	}
}
