package com.creditsuisse.recruitment;

public class InvalidOrderPriceException extends RuntimeException {
	public InvalidOrderPriceException(final String message) {
		super(message);
	}
}
